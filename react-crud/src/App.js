import AddUser from "./components/AddUser";
import Header from "./components/Header";
import UsersDataTable from "./components/UsersDataTable";
import {Route, Switch} from 'react-router-dom';
import EditUser from "./components/EditUser";

function App() {
    return (
        <>
            <Header/>
            <Switch>
                <Route path="/" component={UsersDataTable} exact/>
                <Route path="/users" component={UsersDataTable}/>
                <Route path="/add-user" component={AddUser}/>
                <Route component={EditUser} path="/edit-user/:id" />
            </Switch>
        </>
    );
}

export default App;
