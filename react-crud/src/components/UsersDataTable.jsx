import React, {useState} from "react";
import {Button, Container, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {useDispatch, useSelector} from "react-redux";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faEdit, faTrash} from '@fortawesome/fontawesome-free-solid'
import {Link, useHistory} from "react-router-dom";
import {setUserData} from "../redux/actions/userActions";

const UsersDataTable = () => {

    const [isModalOpen, setIsModalOpen] = useState(false);
    const [deleteId, setDeleteId] = useState(null);
    const dispatch = useDispatch();
    const usersList = useSelector(state => state.usersList.usersList);
    const history = useHistory();

    const handleEdit = (id) => {
        history.push(`/edit-user/${id}`)
    }

    const handleDelete = (id) => {
        setDeleteId(id)
        toggleModal()
    }

    const deleteUser = () => {
        const previousUsers = usersList.filter((e) => e.id !== Number(deleteId));
        dispatch(setUserData(previousUsers))
        toggleModal()
    }

    const toggleModal = () => {
        setIsModalOpen(!isModalOpen)
    }

    const DeleteModal = () => {
        return (
            <div className="text-center">
                <Modal isOpen={isModalOpen} toggle={toggleModal}>
                    <ModalHeader className="text-center">
                        <div className="icon-box">
                            <FontAwesomeIcon className={"red-circle"} icon={faTrash}/>
                        </div>
                        <h2>Are you sure?</h2>
                    </ModalHeader>
                    <ModalBody className="text-center">Do you really want to delete this record?</ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={toggleModal}>Cancel</Button>{' '}
                        <Button color="danger" onClick={deleteUser}>Delete</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
    return (
        <Container>
            <DeleteModal/>
            {
                usersList.length > 0 ?
                    <Table striped bordered hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            usersList && usersList.map(((item, index) => (
                                    <tr>
                                        <td>{index + 1}</td>
                                        <td>{item.firstName}</td>
                                        <td>{item.lastName}</td>
                                        <td>{item.email}</td>
                                        <td>
                                            <div className={"edit-delete-icons"}>
                                                <FontAwesomeIcon
                                                    icon={faEdit}
                                                    className={"edit-icon"}
                                                    title={"Edit User"}
                                                    onClick={() => handleEdit(item.id)}
                                                />
                                                <FontAwesomeIcon
                                                    icon={faTrash}
                                                    className={"delete-icon"}
                                                    title={"Delete User"}
                                                    onClick={() => handleDelete(item.id)}
                                                />
                                            </div>
                                        </td>
                                    </tr>)
                            ))

                        }
                        </tbody>
                    </Table> :
                    <div className={"text-center mt-2"}>
                        <h5>No Data Found</h5>
                        <Link to="/add-user">
                            <Button color={"primary"}>Add User</Button>
                        </Link>

                    </div>
            }

        </Container>)
}
export default UsersDataTable;