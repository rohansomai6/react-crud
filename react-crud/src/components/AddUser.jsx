import React, {useState} from 'react';

import {Container} from 'reactstrap';
import {useDispatch, useSelector} from "react-redux";
import {setUserData} from "../redux/actions/userActions";
import * as emailHelper from "../helpers/emailHelper";
import {useHistory} from "react-router-dom";
import UserForm from "./UserForm";

const AddUser = () => {
    const [newInput, setNewInput] = useState({
        firstName: '',
        lastName: '',
        email: '',
    })
    const [error, setError] = useState({
        status: false,
        message: '',
    });
    const dispatch = useDispatch();
    const history = useHistory();
    const usersList = useSelector(state => state.usersList.usersList);

    const handleOnChange = (e) => {
        setNewInput({...newInput, [e.target.name]: e.target.value})
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        if (!newInput.firstName) {
            setError({status: true, message: "First Name is required!"})
            return
        }
        if (!newInput.lastName) {
            setError({status: true, message: "Last Name is required!"})
            return
        }
        if (!newInput.email) {
            setError({status: true, message: "Email is required!"})
            return
        }
        if (newInput.email && !emailHelper.validateEmail(newInput.email)) {
            setError({status: true, message: "Please enter a valid email"})
            return
        }
        setError({status: false, message: ""});

        dispatch(setUserData([...usersList, {...newInput, id: Math.floor((Math.random() * 100) + 1)}]));

        history.push("/users")

    }

    return (
        <Container>
            <UserForm
                handleOnChange={handleOnChange}
                handleSubmit={handleSubmit}
                updateFlag={false}
                error={error}
                input={newInput}
            />
        </Container>)
}
export default AddUser;