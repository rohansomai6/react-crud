import {Button, Col, Form, FormGroup, Input, Label, Row} from "reactstrap";
import React from "react";

const UserForm = ({updateFlag, handleSubmit, error, handleOnChange, input}) => {
    return <Row className={"d-flex justify-content-center"}>
        <Col md={6}>
            <h3 className={"text-center mt-2 mb-3"}> {`${updateFlag ? "Edit" : "Add"} User`}</h3>
            <Form onSubmit={handleSubmit}>
                {error && error.status && <div className="alert alert-danger" role="alert">
                    {error.message}
                </div>}
                <FormGroup>
                    <Label for="firstName">First Name</Label>
                    <Input type="text" name="firstName" id="firstName" placeholder="Enter First Name"
                           value={input.firstName}
                           onChange={handleOnChange}/>
                </FormGroup>
                <FormGroup>
                    <Label for="lastName">Last Name</Label>
                    <Input type="text" name="lastName" id="lastName" placeholder="Enter Last Name"
                           value={input.lastName}
                           onChange={handleOnChange}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="email">Email</Label>
                    <Input type="email" name="email" id="email" placeholder="Enter Email"
                           value={input.email}
                           onChange={handleOnChange}/>
                </FormGroup>
                <div className="d-grid gap-2">
                    <Button color="success">
                        Submit
                    </Button>

                </div>

            </Form>
        </Col>
    </Row>
}

export default UserForm;