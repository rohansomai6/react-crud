import React from "react";
import {Nav, Navbar, NavbarBrand, NavbarToggler, NavItem} from 'reactstrap';
import {Link} from "react-router-dom";

const Header = () => {

    return (<Navbar color="dark" dark expand="md">
        <NavbarBrand>React CRUD App</NavbarBrand>
        <NavbarToggler/>
        <Nav className="ml-auto" navbar>
            <NavItem>
                <Link to="/users" className={"nav nav-link"}>View all users</Link>
            </NavItem>
            <NavItem>
                <Link to="/add-user" className={"nav nav-link"}>Add user</Link>
            </NavItem>
        </Nav>
    </Navbar>)

}
export default Header