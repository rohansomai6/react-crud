import {Link, useHistory, useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {setUserData} from "../redux/actions/userActions";
import {useDispatch, useSelector} from "react-redux";
import {Button, Col, Container, Form, FormGroup, Input, Label, Row} from "reactstrap";
import * as emailHelper from "../helpers/emailHelper";
import UserForm from "./UserForm";

const EditUser = () => {
    const usersList = useSelector(state => state.usersList.usersList);
    const dispatch = useDispatch();
    const history = useHistory();
    let {id} = useParams();

    const [editInput, setEditInput] = useState({
        firstName: '',
        lastName: '',
        email: '',
    })

    const [error, setError] = useState({
        status: false,
        message: '',
    });


    const handleSubmit = (event) => {
        event.preventDefault();
        if (!editInput.firstName) {
            setError({status: true, message: "First Name is required!"})
            return
        }
        if (!editInput.lastName) {
            setError({status: true, message: "Last Name is required!"})
            return
        }
        if (!editInput.email) {
            setError({status: true, message: "Email is required!"})
            return
        }
        if (editInput.email && !emailHelper.validateEmail(editInput.email)) {
            setError({status: true, message: "Please enter a valid email"})
            return
        }
        setError({status: false, message: ""});

        const previousUsers = usersList.filter((e) => e.id !== Number(id));
        previousUsers.push(editInput);
        dispatch(setUserData(previousUsers))
        history.push('/');
    }

    const handleOnChange = (e) => {
        setEditInput({...editInput, [e.target.name]: e.target.value})
    }

    const loadUserData = (id) => {
        setEditInput(usersList.find((e) => e.id === id))
    }
    useEffect(() => {
        loadUserData(Number(id));
    }, [])
    return (
        <Container>
            {editInput ?
                <UserForm
                    handleOnChange={handleOnChange}
                    handleSubmit={handleSubmit}
                    updateFlag={true}
                    error={error}
                    input={editInput}
                /> :
                <div className={"text-center mt-2"}>
                    <h5>The user you requested does not found in ou records.</h5>
                </div>
            }
        </Container>)
}
export default EditUser