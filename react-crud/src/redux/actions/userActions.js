export const SET_USER_DATA = "SET_USER_DATA";

export function setUserData(usersList) {
    return {
        type: SET_USER_DATA,
        usersList: usersList
    };
}
