import {SET_USER_DATA} from "../actions/userActions";

const initialState = {
    usersList: []
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_USER_DATA:
            return {...state, usersList: action.usersList};
        default:
            return state;
    }
}
