import {combineReducers, createStore} from "redux";
import {
    userReducer
} from "./reducers";
import {composeWithDevTools} from "redux-devtools-extension";

const reducer = combineReducers({
    usersList: userReducer
});

const store = createStore(reducer, composeWithDevTools());

export default store;
